terraform {
  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
      version = "15.9.0"
    }
  }
}

provider "gitlab" {
  # Configuration options
  token = "${var.gitlab_token}"
}

resource "gitlab_group" "d7007" {
  # (resource arguments)
  auto_devops_enabled                = false
  default_branch_protection          = 2
  description                        = "updated from terraform i1"
  emails_disabled                    = false
  extra_shared_runners_minutes_limit = 0
  # read only -- full_name                          = "Off Hours DevOps"
  # read only -- full_path                          = "d7007"
  # read only -- id                                 = "39808416"
  ip_restriction_ranges              = []
  lfs_enabled                        = true
  membership_lock                    = false
  mentions_disabled                  = false
  name                               = "Off Hours DevOps"
  parent_id                          = 0
  path                               = "d7007"
  prevent_forking_outside_group      = false
  project_creation_level             = "maintainer"
  request_access_enabled             = false
  require_two_factor_authentication  = false
  # read only -- runners_token                      = (sensitive value)
  share_with_group_lock              = false
  shared_runners_minutes_limit       = 0
  subgroup_creation_level            = "owner"
  two_factor_grace_period            = 48
  visibility_level                   = "private"
  # read only -- web_url                            = "https://gitlab.com/groups/d7007"
}


